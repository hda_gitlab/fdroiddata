Disabled:Not Free Software, see https://github.com/Glucosio/glucosio-android/issues/219#issuecomment-204803228
AntiFeatures:UpstreamNonFree
Categories:Sports & Health
License:GPLv3
Web Site:https://glucosio.org/
Source Code:https://github.com/Glucosio/android
Issue Tracker:https://github.com/Glucosio/android/issues

Auto Name:Glucosio
Summary:Manage your diabetes
Description:
Manage and research diabetes.
.

Repo Type:git
Repo:https://github.com/Glucosio/android

Build:1.0,1
    commit=3391956e77b391e4e759ad4b607dad7a3e68c8c6
    subdir=app
    gradle=yes

Build:0.8.1,2
    commit=540384a2acda7421a344f686258758c4898900d9
    subdir=app
    gradle=yes

Build:0.8.2,3
    disable=play-services
    commit=0.8.2
    subdir=app
    gradle=yes

Build:0.10.3,18
    disable=play-services
    commit=0.10.3
    subdir=app
    gradle=yes

Maintainer Notes:
See https://github.com/Glucosio/android/issues/122
.

Archive Policy:0 versions
Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0.2
Current Version Code:26
