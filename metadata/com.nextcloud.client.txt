Categories:Internet
License:GPLv2
Web Site:https://www.nextcloud.com
Source Code:https://github.com/nextcloud/android
Issue Tracker:https://github.com/nextcloud/android/issues
Changelog:https://github.com/nextcloud/android/blob/HEAD/CHANGELOG.md
Donate:https://www.bountysource.com/teams/nextcloud

Auto Name:Nextcloud
Summary:Synchronization client
Description:
A safe home for all your data. Access & share your files, calendars, contacts,
mail & more from any device, on your terms. This is the official Android app.
.

Repo Type:git
Repo:https://github.com/nextcloud/android.git

Build:1.0.0,10000000
    commit=stable-1.0.0
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Build:1.0.1,10000100
    commit=stable-1.0.1
    submodules=yes
    gradle=yes
    rm=libs/disklrucache*,user_manual

Auto Update Mode:Version stable-%v
Update Check Mode:Tags ^stable
Current Version:1.0.1
Current Version Code:10000100
