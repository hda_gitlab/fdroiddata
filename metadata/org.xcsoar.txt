Categories:Navigation
License:GPLv2
Web Site:http://www.xcsoar.org
Source Code:http://git.xcsoar.org/cgit/master/xcsoar.git
Issue Tracker:http://bugs.xcsoar.org
Changelog:http://git.xcsoar.org/cgit/master/xcsoar.git/tree/NEWS.txt

Auto Name:XCSoar
Summary:Tactical glide computer and maps
Description:
XCSoar is a tactical glide computer for soaring and para glider pilots. It
supports navigation, airspace warnings, final glide calculations, wind
calculation, collision avoidance and many many more features.
.

Repo Type:git
Repo:git://git.xcsoar.org/xcsoar/master/xcsoar.git

Build:6.7.3,92
    commit=v6.7.3
    subdir=android
    submodules=yes
    output=../output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk
    build=cd .. && \
        for n in 1 2 3; do make TARGET=ANDROIDFAT DEBUG=n TESTING=n -j4 output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk && \
        break; done

Build:6.7.4,93
    commit=v6.7.4
    subdir=android
    submodules=yes
    output=../output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk
    build=cd .. && \
        for n in 1 2 3; do make TARGET=ANDROIDFAT DEBUG=n TESTING=n -j4 output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk && \
        break; done

Build:6.7.5,94
    commit=v6.7.5
    subdir=android
    submodules=yes
    output=../output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk
    build=cd .. && \
        for n in 1 2 3; do make TARGET=ANDROIDFAT DEBUG=n TESTING=n -j4 output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk && \
        break; done

Build:6.7.6,95
    commit=v6.7.6
    subdir=android
    submodules=yes
    output=../output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk
    build=cd .. && \
        for n in 1 2 3; do make TARGET=ANDROIDFAT DEBUG=n TESTING=n -j4 output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk && \
        break; done

Build:6.7.7,96
    commit=v6.7.7
    subdir=android
    submodules=yes
    output=../output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk
    build=cd .. && \
        for n in 1 2 3; do make TARGET=ANDROIDFAT DEBUG=n TESTING=n -j4 output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk && \
        break; done

Build:6.7.8,97
    commit=v6.7.8
    subdir=android
    submodules=yes
    output=../output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk
    build=cd .. && \
        for n in 1 2 3; do make TARGET=ANDROIDFAT DEBUG=n TESTING=n -j4 output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk && \
        break; done

Build:6.7.9,98
    commit=v6.7.9
    subdir=android
    submodules=yes
    output=../output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk
    build=cd .. && \
        for n in 1 2 3; do make TARGET=ANDROIDFAT DEBUG=n TESTING=n -j4 output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk && \
        break; done

Build:6.8,99
    commit=v6.8
    subdir=android
    submodules=yes
    output=../output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk
    build=cd .. && \
        for n in 1 2 3; do make TARGET=ANDROIDFAT DEBUG=n TESTING=n -j4 output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk && \
        break; done

Build:6.8.1,100
    commit=v6.8.1
    subdir=android
    submodules=yes
    output=../output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk
    build=cd .. && \
        for n in 1 2 3; do make TARGET=ANDROIDFAT DEBUG=n TESTING=n -j4 output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk && \
        break; done

Build:6.8.2,101
    commit=v6.8.2
    subdir=android
    submodules=yes
    output=../output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk
    build=cd .. && \
        for n in 1 2 3; do make TARGET=ANDROIDFAT DEBUG=n TESTING=n -j4 output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk && \
        break; done

Build:6.8.3,102
    commit=v6.8.3
    subdir=android
    submodules=yes
    output=../output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk
    build=cd .. && \
        for n in 1 2 3; do make TARGET=ANDROIDFAT DEBUG=n TESTING=n -j4 output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk && \
        break; done

Build:6.8.5,105
    commit=v6.8.5
    subdir=android
    submodules=yes
    output=../output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk
    build=cd .. && \
        for n in 1 2 3; do make TARGET=ANDROIDFAT DEBUG=n TESTING=n -j4 output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk && \
        break; done

Maintainer Notes:
The makefile setup is not perfect, so we need to run it a few times to get it to succeed.
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:6.8.5
Current Version Code:105
