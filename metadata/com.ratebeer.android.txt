Disabled:non-free, little chance we'll ever build it
Categories:Internet
License:GPLv3+
Web Site:http://www.ratebeer.com
Source Code:https://github.com/erickok/ratebeer
Issue Tracker:https://github.com/erickok/ratebeer/issues

Auto Name:RateBeer
Summary:RateBeer.com client
Description:
Companion client for the [http://www.ratebeer.com RateBeer]-Website.

Features:

* Search and explore (top) beers without the need for an account
* On- and off-line rating of beers
* Offer place and event suggestions based on a user's geographical location
.

Repo Type:git
Repo:https://github.com/erickok/ratebeer

Build:1.5.8,52
    disable=filled with jars, uses google play services lib
    commit=none
    subdir=RateBeerForAndroid

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.0.6
Current Version Code:210
